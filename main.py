from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions

from src.parse_market_data import parse_market_data
from src.very_long_human_behaviour import behaviour_like_human

if __name__ == '__main__':
    options = ChromeOptions()

    options.add_argument("start-maximized")
    # Set antiblocking values for webdriver
    options.add_argument('--disable-blink-features=AutomationControlled')

    with Chrome(options=options, executable_path=r'C:\Program Files (x86)\Selenium\chromedriver.exe') as browser:
        browser.get("https://www.nseindia.com")
        parse_market_data(browser)
        behaviour_like_human(browser)
