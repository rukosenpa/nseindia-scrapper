import random

from selenium.webdriver import ActionChains
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as exp_conds
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

from src.utils import get_scroll_options

BANNER_TEXT_SELECTOR = 'div.banner_text'
HOME_LINK_SELECTOR = '#main_navbar>ul>li:nth-child(1)>a'
NIFTY_BANK_SELECTOR = 'a.nav-item.nav-link[href="#NIFTY BANK"]'
VIEW_ALL_LINK_SELECTOR = '#tab4_gainers_loosers>div.link-wrap>a'

INDICES_SELECTOR_SELECTOR = '#equitieStockSelect'
# INDICES_TABLE_CONTAINER_XPATH = '//*[@id="equity-stock"]/div[4]/div'
INDICES_TABLE_ROW_SELECTOR = '#equityStockTable>tbody>tr'


def behaviour_like_human(browser: Chrome):
    wait = WebDriverWait(browser, 10)

    # Go to main page via navigation
    home_link = browser.find_element_by_css_selector(HOME_LINK_SELECTOR)
    ActionChains(browser).move_to_element(
        home_link
    ).click(
        home_link
    ).perform()

    wait.until(
        exp_conds.visibility_of_element_located((By.CSS_SELECTOR, BANNER_TEXT_SELECTOR))
    )

    ActionChains(browser).pause(
        random.randint(2, 7) + random.random()
    ).perform()

    browser.execute_script(f'window.scroll({get_scroll_options(0, 500)})')

    nifty_bank_link = wait.until(
        exp_conds.visibility_of_element_located((By.CSS_SELECTOR, NIFTY_BANK_SELECTOR))
    )

    ActionChains(browser).pause(
        random.randint(2, 7) + random.random()
    ).move_to_element(
        nifty_bank_link
    ).click(
        nifty_bank_link
    ).pause(3).perform()

    browser.execute_script(f'window.scroll({get_scroll_options(0, 750)})')
    ActionChains(browser).pause(1.3).perform()

    view_all_link = browser.find_element_by_css_selector(VIEW_ALL_LINK_SELECTOR)

    # I get ElementIsNotInteractable exception every time, but it is a link and it can't be not interactable
    # ActionChains(browser).move_to_element_with_offset(
    #     view_all_link, 10, 10
    # ).pause(5).click(
    #     view_all_link
    # ).perform()
    browser.get(view_all_link.get_attribute('href'))

    selector = wait.until(
        exp_conds.visibility_of_element_located((By.CSS_SELECTOR, INDICES_SELECTOR_SELECTOR))
    )
    indices_selector = Select(selector)

    ActionChains(browser).pause(
        random.randint(2, 7) + random.random()
    ).perform()
    indices_selector.select_by_visible_text('NIFTY ALPHA 50')

    # Wait until rows download
    WebDriverWait(browser, 10).until(
        exp_conds.visibility_of_element_located((By.CSS_SELECTOR, INDICES_TABLE_ROW_SELECTOR))
    )

    browser.execute_script(f"window.scroll({get_scroll_options(0, 350)})")
    ActionChains(browser).pause(random.randint(2, 5)).perform()
    row_num = len(browser.find_elements_by_css_selector(INDICES_TABLE_ROW_SELECTOR))

    for i in range(1, row_num // 10):
        ActionChains(browser).pause(
            random.randint(2, 5) + random.random()
        ).perform()
        browser.execute_script(f"""
            table_div = $('#equity-stock > div.tbl_leftcol_fix > div')[0];
            table_div.scroll({get_scroll_options(0, 400 * i)})
        """)
