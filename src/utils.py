def get_scroll_options(x: int, y: int, behaviour='smooth'):
    return str({
        'left': x,
        'top': y,
        'behavior': behaviour
    })
