import random

from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as exp_conds
from selenium.webdriver.support.wait import WebDriverWait

from src.utils import get_scroll_options

MARKET_DATA_LINK_SELECTOR = '#main_navbar>ul>li:nth-child(3)>a'
PREOPEN_MARKET_LINK_SELECTOR = 'a.nav-link[href="/market-data/pre-open-market-cm-and-emerge-market"]'

PRICE_TABLE_CONTAINER_SELECTOR = '#preopen-market>div>div.table-wrap'
STOCK_ROW_SELECTOR = '#livePreTable>tbody>tr'

STOCK_ROW_SELECTOR_TEMPLATE = '#livePreTable>tbody>tr:nth-child({row_number})'
NAME_CELL_SELECTOR_TEMPLATE = STOCK_ROW_SELECTOR_TEMPLATE + '>' + 'td:nth-child(2)>a'
PRICE_CELL_SELECTOR_TEMPLATE = STOCK_ROW_SELECTOR_TEMPLATE + '>' + 'td:nth-child(7)'


def parse_market_data(browser: Chrome):
    wait = WebDriverWait(browser, 10)
    # Move to premarket data page
    ActionChains(
        browser
    ).move_to_element(
        browser.find_element_by_css_selector(MARKET_DATA_LINK_SELECTOR)
    ).perform()

    # Wait until link make visible
    preopen_market_link = wait.until(
        exp_conds.visibility_of_element_located((By.CSS_SELECTOR, PREOPEN_MARKET_LINK_SELECTOR))
    )

    # Click on pre-market link
    ActionChains(
        browser
    ).move_to_element(
        preopen_market_link
    ).pause(
        0.5 + random.random()
    ).click(
        preopen_market_link
    ).perform()

    with open('PreMarketPrices.csv', mode='w') as file:
        data_container = []
        browser.execute_script(f'window.scroll({get_scroll_options(0, 500)});')
        wait.until(
            exp_conds.visibility_of_element_located(
                (By.CSS_SELECTOR, STOCK_ROW_SELECTOR_TEMPLATE.format(row_number=7))
            )
        )
        row_num = len(browser.find_elements_by_css_selector(STOCK_ROW_SELECTOR))
        for i in range(1, row_num + 1):
            if i % 10 == 0:
                # Scrolling in the table every 10 rows
                ActionChains(browser).pause(3.4 + random.random()).perform()
                browser.execute_script(f'''
                    table_div = $('{PRICE_TABLE_CONTAINER_SELECTOR}')[0];
                    table_div.scroll({get_scroll_options(0, 400 * i // 10)});
                ''')

            name_cell = wait.until(
                exp_conds.visibility_of_element_located(
                    (By.CSS_SELECTOR, NAME_CELL_SELECTOR_TEMPLATE.format(row_number=i))
                )
            )
            price_cell = wait.until(
                exp_conds.visibility_of_element_located(
                    (By.CSS_SELECTOR, PRICE_CELL_SELECTOR_TEMPLATE.format(row_number=i))
                )
            )

            name, price = name_cell.text, price_cell.text
            data_container.append(name + ';' + price + '\n')

        file.writelines(data_container)
    return
